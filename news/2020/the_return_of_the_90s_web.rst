

.. _the_return_of_the_90s_web:

==========================
the-return-of-the-90s-web
==========================

.. seealso::

   - https://mxb.dev/blog/the-return-of-the-90s-web/
   - https://gomakethings.com/always-bet-on-html/
   - https://www.phoenixframework.org/
   - https://hey.com/how-it-works/
   - https://webflow.com/


.. contents::
   :depth: 3


Serverside Rendering
======================

After spending the better part of the last decade shifting rendering
logic to the client, it looks like the pendulum is about to swing into
the other direction again.

With projects like Phoenix Liveview or hey.com’s recent “it’s-just-HTML”
approach, it seems like server-side rendering (SSR) is stepping back
into the spotlight.


It makes sense - servers are really good at this, and sending compressed
HTML through the network can be lightning fast.

The classic request-response cycle has evolved as well: HTTP/2 and smart
techniques like Turbolinks or just-in-time preloading allow for a much
better experience now than when you first tried to load that Michael
Jordan photo on the Space Jam website over dial-up.

Taking the responsibility of rendering UI and all the Javascript that
comes with it off users’ shoulders would be a great old new strategy
for the next generation of web apps.

Today our understanding of the web has improved, and so have our tools.
Webflow is one of the contenders for the “no-code editor” trophy.
The output it generates is far better.

These tools will probably not be a replacement for developers as a
whole - complex projects still require tons of human brainpower to work.

But for all the landing pages and marketing sites, this could be the
holy grail of WYSIWYG we thought we had in the 90s.
