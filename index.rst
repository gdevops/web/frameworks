.. Documentation tutorial documentation master file
   2019-04-27

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/frameworks/rss.xml>`_

.. _tuto_webframeworks:

==========================
Webframeworks tutorial
==========================

.. figure:: images/web_frameworks.jpg
   :align: center

- https://gitlab.com/gdevops
- https://framagit.org/gdevops/web/frameworks
- https://gdevops.frama.io/web/frameworks/
- https://github.com/the-benchmarker/web-frameworks
- :ref:`genindex`

.. toctree::
   :maxdepth: 4
   :caption: Web frameworks news

   news/news

.. toctree::
   :maxdepth: 4
   :caption: Frameworks

   javascript/javascript
   python/python
   ruby/ruby


