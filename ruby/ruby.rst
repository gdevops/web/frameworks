
.. index::
   pair: Ruby ; Frameworks

.. _ruby_frameworks:

=======================
Ruby frameworks
=======================

.. toctree::
   :maxdepth: 3

   rails/rails
