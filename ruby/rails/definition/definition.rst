
.. _rails_def:

==========================
Rails definition
==========================

.. contents::
   :depth: 3

github rails definition
==========================

.. seealso::

   - https://github.com/rails/rails/blob/master/README.md


.. include:: README.rst
