
.. index::
   pair: Ruby; Ruby On Rails
   pair: Ruby; Rails
   ! Rails
   ! Ruby On Rails

.. _rails:

==========================
Rails (Ruby on Rails)
==========================

.. seealso::

   - https://en.wikipedia.org/wiki/Ruby_on_Rails
   - https://github.com/rails/rails
   - https://rubyonrails.org/
   - https://x.com/rails
   - https://x.com/dhh


.. figure:: rails_logo.svg
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
