
.. index::
   pair: Python ; FastAPI
   ! FastAPI

.. _framework_fastapi:

=============================================================================================
**FastAPI** framework, high performance, easy to learn, fast to code, ready for production
=============================================================================================

.. seealso::

   - https://github.com/tiangolo/fastapi
   - https://github.com/tiangolo/fastapi/network/dependencies
   - https://github.com/the-benchmarker/web-frameworks/tree/master/python/fastapi
   - https://fastapi.tiangolo.com/
   - https://x.com/tiangolo
   - https://github.com/tiangolo/full-stack-fastapi-postgresql
   - https://github.com/tiangolo
   - https://dockerswarm.rocks/


.. figure:: logo_fastapi.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 6

   definition/definition
   django/django
   versions/versions
