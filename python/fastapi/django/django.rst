
.. index::
   pair: Python ; FastAPI
   ! FastAPI

.. _fastapi_django:

=================================================
**FastAPI** and Django
=================================================

.. seealso::

   - https://github.com/erm/django-fastapi-example
   - :ref:`django_fastapi_2020_05_02`
