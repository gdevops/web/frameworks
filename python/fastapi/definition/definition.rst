
.. index::
   pair: Definition ; Fastapi


.. _fastapi_definition:

============================================
Fastapi definition
============================================

.. contents::
   :depth: 3


README.md
===========

.. seealso::

   - https://github.com/tiangolo/fastapi/blob/master/README.md

.. include:: README.rst

https://blog.ippon.fr/2021/10/27/fastapi/
===========================================

- https://blog.ippon.fr/2021/10/27/fastapi/

FastAPI est un framework pour la création d’API (synchrone ou non) avec
Python 3.6+.

Le framework a été créé par Sebastian Ramirez en 2018.
Et bien que FastAPI soit jeune, il tient déjà de grandes promesses, et
souhaite moderniser les frameworks orientés API.

Pour cela, ce framework nouvelle génération mise sur plusieurs points clés :

- Rapidité : des performances à même de rivaliser avec NodeJS et Go (grâce à Starlette et Pydantics).
- Développement : simplicité de Python et framework intuitif.
- Robuste : code prêt pour la production.
- Standardisé : basé (et complètement compatible) avec des standards open source, notamment OpenAPI et JSON schema.
- Documentation : pas besoin d'ajout de code, elle est automatiquement générée à partir de l'existant.
