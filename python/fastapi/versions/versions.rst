.. index::
   pair: Versions ; Fastapi

.. _fastapi_versions:

============================================
Fastapi versions
============================================

.. seealso::

   - https://github.com/tiangolo/fastapi/releases

.. toctree::
   :maxdepth: 6

   0.61.1/0.61.1
   0.54.1/0.54.1
   0.52.0/0.52.0
   0.49.0/0.49.0
   0.30.0/0.30.0
   0.20.0/0.20.0
   0.17.0/0.17.0
