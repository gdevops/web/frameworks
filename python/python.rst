
.. index::
   pair: Python ; Webframeworks
   ! Python Webframeworks

.. _python_webframeworks:

==========================
Python Webframeworks
==========================

.. seealso::

   - https://github.com/the-benchmarker/web-frameworks/tree/master/python

.. toctree::
   :maxdepth: 4

   articles/articles
   django/django
   fastapi/fastapi
