.. index::
   pair: FastAPI ; 2020-04

.. _fastapi_2020_04:

===========================================================================================
Build The Next Generation Of Python Web Applications With FastAPI with Sebastián Ramírez
===========================================================================================

.. seealso::

   - https://www.pythonpodcast.com/fastapi-web-application-framework-episode-259/
   - https://x.com/tiangolo


.. contents::
   :depth: 3

Summary
==========

Python has an embarrasment of riches when it comes to web frameworks,
each with their own particular strengths.

FastAPI is a new entrant that has been quickly gaining popularity as a
performant and easy to use toolchain for building RESTful web services.

In this episode Sebastián Ramirez shares the story of the frustrations
that led him to create a new framework, how he put in the extra effort
to make the developer experience as smooth and painless as possible,
and how he embraces extensability with lightweight dependency injection
and a straightforward plugin interface.

If you are starting a new web application today then FastAPI should be
at the top of your list.


An HTTP server to display desktop notifications
===================================================

.. seealso::

   - https://julienharbulot.com/notification-server.html#python-code-fastapi
   - https://github.com/julien-h/http-notification-server
