

.. index::
   pair: Lean web ; Vanilla Javascript


.. _lean_web:

===========================================
The **Lean web** #leanweb
===========================================

.. seealso::

   - https://gomakethings.com/talks/the-lean-web/
   - https://speakerdeck.com/cferdinandi/the-lean-web
   - https://gomakethings.com/the-importance-of-fundamentals/
   - https://gomakethings.com/why-javascript-is-the-pollution-of-the-web/


.. contents::
   :depth: 3


To summarize
==============

.. figure::the_lean_web_summarize.png
   :align: center

   https://speakerdeck.com/cferdinandi/the-lean-web?slide=98


Description
=============

Front end development has gotten a **lot more complicated**.

Preprocessors, package managers, command line tools, CSS-in-JS, the never-ending
stream of JS Frameworks... **it’s exhausting**.

But there’s a simpler way !

In this talk, Chris explores how modern best practices are making it harder to
build things for the web. You’ll learn new approaches and techniques that you
can use to more easily build modern, engaging websites and web apps… without
all of the cruft.

All you need is a web browser and a text editor.


This talk explores
=====================


This talk explores:

- Why many of today’s best practices exist.
- How the explosive use of JavaScript has had a negative impact on web
  performance and the user experience.
- Whether or not modern web development tools actually improve the developer
  experience.
- Simpler and more performant alternatives to some of today’s more popular
  best practices.

