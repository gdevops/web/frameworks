
.. index::
   pair: Chris Ferdinandi ; Javascript people

.. _chris_ferdinandi:

===========================================
Chris Ferdinandi
===========================================

.. seealso::

   - https://x.com/ChrisFerdinandi
   - https://vanillajsguides.com/
   - https://gomakethings.com/articles/
   - https://gomakethings.com/why-javascript-is-the-pollution-of-the-web/


.. figure:: chris-ferdinandi-high-res.jpg
   :align: center

   https://x.com/ChrisFerdinandi


.. toctree::
   :maxdepth: 3

   lean_web/lean_web
