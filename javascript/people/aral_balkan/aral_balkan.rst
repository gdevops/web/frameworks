
.. index::
   pair: Aral Balkan ; Javascript people

.. _aral_balkan:

===========================================
Aral Balkan
===========================================

.. seealso::

   - :ref:`small_tech`
   - https://x.com/aral
   - https://ar.al/
   - https://2017.ind.ie/ethical-design/
   - https://mastodon.ar.al/about


.. figure:: aral_balkan.png
   :align: center

   https://ar.al/
