
.. index::
   pair: Laura Kalbag ; Javascript people

.. _laura_kalbag:

===========================================
Laura Kalbag
===========================================

.. seealso::

   - :ref:`small_tech`
   - https://x.com/laurakalbag
   - https://abookapart.com/products/accessibility-for-everyone
   - https://2017.ind.ie/ethical-design/


.. figure:: laura-kalbag.jpg
   :align: center


.. contents::
   :depth: 3

About Laura Kalbag (source https://abookapart.com/products/accessibility-for-everyone)
========================================================================================

Laura Kalbag is a designer from the UK. She’s one third of Ind.ie, a tiny
two-person-and-one-husky social enterprise working for social justice in the
digital age. At Ind.ie, Laura follows the `Ethical Design Manifesto`_, and works
on a web privacy tool called Better.

Laura does everything from design to development while learning how to run a
sustainable social enterprise.

She strives to make privacy and broader ethics in technology accessible to a wide
audience.

You can typically find her making design decisions, writing CSS, nudging icon
pixels, or distilling a privacy policy into something humans can understand.

Sometimes, she speaks at conferences and writes articles, too.


.. _`Ethical Design Manifesto`: https://2017.ind.ie/ethical-design/
