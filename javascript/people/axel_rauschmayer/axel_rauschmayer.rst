
.. index::
   pair: Axel Rauschmayer ; Javascript people

.. _axel_rauschmayer:

===========================================
Axel Rauschmayer
===========================================

.. seealso::

   - https://x.com/rauschma
   - https://github.com/rauschma
