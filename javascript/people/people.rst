.. index::
   pair: Javascript ; People

.. _javascript_people:

=======================
Javascript people
=======================

.. seealso::

   - :ref:`javascript_tuto`


.. toctree::
   :maxdepth: 3


   aral_balkan/aral_balkan
   axel_rauschmayer/axel_rauschmayer
   chris_ferdinandi/chris_ferdinandi
   laura_kalbag/laura_kalbag

