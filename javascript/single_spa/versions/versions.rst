

.. index::
   pair: single-spa ; Versions

.. _single_spa_versions:

===========================================
single-spa versions
===========================================

.. seealso::

   - https://github.com/CanopyTax/single-spa/releases

.. toctree::
   :maxdepth: 3

   4.3.5/4.3.5
