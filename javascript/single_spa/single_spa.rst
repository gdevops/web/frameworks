

.. index::
   pair: single-spa ; Framework

.. _single_spa:

=========================================================================
single-spa (Microfrontends made easy, single-spa is a top level router)
=========================================================================

.. seealso::

   - https://github.com/CanopyTax/single-spa
   - https://single-spa.js.org/


.. figure:: single_spa.svg
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 3

   versions/versions
