.. index::
   pair: Technical ; Debt

.. _web_javascript_technical_debt:

================================================================================
Problems: the valse of Javascript frameworks : the technical debt
================================================================================

- :ref:`tuto_htmx:backends_http_servers`

Introduction
==============

Depuis plusieurs mois et meme années certains développeurs se posent des
questions de base sur le développement des applications Web avec les derniers
frameworks Web qui sont devenus très complexes et qui étonnent **beaucoup**
les développeurs "classiques" mais aussi les développeurs Web professionnels
comme https://x.com/ChrisFerdinandi et bien d'autres.

- https://www.petelambert.com/journal/html-is-the-web/
- https://gomakethings.com/the-importance-of-fundamentals/


MVC frameworks like Django, Spring, etc. are going to make a massive comeback in the next decade
===================================================================================================

- https://x.com/htmx_org/status/1523654306204856321?s=20&t=HdAUvzdd9uxpPJCGKAkK_w

.. figure:: images/django_come_back.png
   :align: center


MVC frameworks like Django, Spring, etc. are going to make a massive
comeback in the next decade

the hypermedia architecture is just starting to reemerge & these tools
are *perfectly* positioned to be the backend for this shift

the once & future kings!

when I say comeback, I don't mean that they aren't being used today: many
folks are still using them as JSON terminals talking to a SPA front end

what I mean is that all the old, great server side infrastructure (e.g. templates)
currently laying fallow will spring back to life

as the `article says <https://www.david-dahan.com/blog/10-reasons-mvc-frameworks-arent-dinosaurs-but-sharks>`_,
these tools have been honed to a razors edge by decades of template
rendered web 1.0 apps

once people realize what you can achieve w/ hypermedia & how much less
complex it is, it will be off to the races

bullish on 2005 tech books!


10 reasons MVC frameworks aren't dinosaurs but sharks
=========================================================

- https://www.david-dahan.com/blog/10-reasons-mvc-frameworks-arent-dinosaurs-but-sharks


.. figure:: images/django_shark.png
   :align: center


I first heard about the dinosaur 🦕 VS shark 🦈 analogy when I read this
`excellent article <https://www.simplethread.com/relational-databases-arent-dinosaurs-theyre-sharks/>`_ about relational databases:

You see, relational databases aren’t dinosaurs. They aren’t lumbering
prehistoric relics doomed to extinction by a changing world.

They are sharks. Apex predators honed by millions of years of evolution
into a perfectly adapted creature that is just as effective today as it
was eons ago.




HTMX daily reminder Front end devs thinkin' everyone is gonna be using javascript on the back end soon
=======================================================================================================

Front end devs thinkin' everyone is gonna be using javascript on the
back end soon.

.. figure:: htmx/htmx_is_safe.png
   :align: center

   https://x.com/htmx_org/status/1449028339042291718?s=20



It's difficult to get a man undestand something when his salary depends upon his not understanding it
=======================================================================================================

It's difficult to get a man undestand something when his salary depends
upon his not understanding it

.. figure:: salary_depends/upton_sinclair..png
   :align: center

   https://x.com/htmx_org/status/1449345690900049921?s=20


The cost of javascript frameworks
===================================

.. toctree::
   :maxdepth: 3

   timkadlec/timkadlec


Thoughts from https://x.com/getify
===========================================

.. figure:: images/javascript_first_user_experience_last.jpeg
   :align: center

   https://x.com/getify/status/1262232370343825408?s=20


How Modern JavaScript is Ruining the Web w/Chris Ferdinandi (2021-07)
=========================================================================

- https://dev.to/mikhailkaran/how-modern-javascript-is-ruining-the-web-w-chris-ferdinandi-5eo3

What's This One About ?

In this episode Matt and Mike sit down with Chris Ferdinandi to discuss
the current state of JavaScript and more specifically why Chris thinks
that it's ruining the web as we know it.

JavaScript has exploded in popularity over the past few years and with
that a rush of new developer talent has adopted the likes of JavaScript
frameworks (ie React, Vue) to spin up projects quickly and easily, even
if they're not that big (ie a landing page).

In addition to this conversation, the trio discuss the importance of
documentation, accessibility, and more !


Thoughts from (from https://x.com/jaredpalmer)
======================================================

- https://x.com/jaredpalmer

Javascript feelings
---------------------

.. seealso::

   - https://x.com/jaredpalmer/status/1142976821828497409

.. figure:: javascript_feelings/javascript_feelings.png
   :align: center

   https://x.com/jaredpalmer/status/1142976821828497409


Modern development
---------------------

- https://x.com/jaredpalmer/status/1142800704580591617

.. figure:: modern_development/modern_development.png
   :align: center

   https://x.com/jaredpalmer/status/1142800704580591617


This is fine
=================

.. figure:: this_is_fine/this_is_fine.png
   :align: center

   https://x.com/devsimplicity/status/1448635617714266114?s=20


.. _javascript_framework_risks:

Javascript framework risks
==============================


.. figure:: risks/javascript.png
   :align: center

   https://discord.com/channels/725789699527933952/725789747212976259/851942686205673503


.. _react_overengineering:

React overengineering
========================


.. figure:: react/over_engineering.png
   :align: center

   https://x.com/guettli/status/1402512641231306752?s=20



Les spécialistes front-end en faveur de la simplicité
========================================================

Chris Ferdinandi (https://x.com/ChrisFerdinandi)
--------------------------------------------------------

- https://gomakethings.com/why-javascript-is-the-pollution-of-the-web/
- https://gomakethings.com/articles/
- https://vanillajsguides.com/


Andy bell
-----------

- https://andy-bell.design/wrote/keep-it-simple/


https://x.com/peterjlambert
----------------------------------------

- https://www.petelambert.com/journal/html-is-the-web/


https://x.com/Rich_Harris
---------------------------------

- créateur de sveltejs.

- https://svelte.dev/
- https://dev.to/richharris/why-i-don-t-use-web-components-2cia

Sam et max sur la dette technique
-------------------------------------


- http://sametmax.com/la-communaute-js-est-actuellement-une-machine-a-creer-de-la-dette-technique/


JeremyMouzin sur les récents frameworks Web
=============================================

- https://x.com/JeremyMouzin
- https://x.com/JeremyMouzin/status/1142000610474168320


Intéressant de penser que @vuejs va être le framework "final" qui mettra tout
le monde d'accord.
Mais il y en a d'autres qui arrivent comme @sveltejs et d'autres qu'on ne
connait pas encore .. La valse des frameworks JS ne s'arrêtera jamais...

.. figure:: valse/la_valse.png
   :align: center

   https://x.com/JeremyMouzin/status/1142000610474168320



Articles sur la dette technique produit par les nouveaux frameworks Javascript
=================================================================================

- https://www.wax-interactive.com/fr-ch/frameworks-frontend-bonne-intention-dette-technique


.. figure:: images/les_problemes.png
   :align: center


New Javascript frameworks
============================


- https://x.com/burkeholland/status/1130849655598735362

.. figure:: new_javascript_framework/new_javascript_framework.png
   :align: center


Katerina Borodina (What the fuck !)
====================================


- https://x.com/kathyra\_
- https://x.com/kathyra\_/status/1139414621163421697


.. figure:: katerina_borodina/what_the_fuck.png
   :align: center

   https://x.com/kathyra\_/status/1139414621163421697


Moore law
===========

- https://x.com/scottjehl/status/1373643473216274434?s=20

Websites will continuously add more JavaScript to neutralize the performance
improvements predicted by Moore's Law.


.. figure:: moore_law/moore_llaw.png
   :align: center


Getting them used to the pain of the #javascript life as early as possible
==============================================================================

.. figure:: for_kids/for_kids.png
   :align: center

   https://x.com/notAnotherVivek/status/1447415920746381315?s=20

JS build
============

- https://htmx.org/discord

.. figure:: js_build/js_build.png
   :align: center

   https://discord.com/channels/725789699527933952/725789747212976259/898032176786968597


So hard to maintain
=====================

.. figure:: dumpsterfire/so_hard_to_maintain.png
   :align: center

   https://x.com/CrowdSentry/status/1450545267774607362?s=20
