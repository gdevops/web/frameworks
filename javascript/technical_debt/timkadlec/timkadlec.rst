.. index::
   pair: Technical ; Debt

.. _javascript_cost:

==================================================================
The cost of javascript frameworks by https://x.com/tkadlec
==================================================================

.. seealso::

   - https://timkadlec.com/remembers/2020-04-21-the-cost-of-javascript-frameworks/
   - https://x.com/tkadlec
   - https://javascript.developpez.com/actu/303519/Quels-sont-les-couts-lies-a-l-utilisation-de-frameworks-JavaScript-pour-le-developpement-Web-Une-analyse-des-sites-utilisant-React-Vue-js-ou-Angular/

.. contents::
   :depth: 3


Introduction
==============

There is no faster (pun intended) way to slow down a site than to use a
bunch of JavaScript.

The thing about JavaScript is you end up paying a performance tax no
less than four times:

- The cost of downloading the file on the network
- The cost of parsing and compiling the uncompressed file once downloaded
- The cost of executing the JavaScript
- The memory cost

The combination is very expensive.


And we are shipping an increasingly high amount. We’re making the core
functionality of our sites increasingly dependant on JavaScript as
organizations move towards sites driven by frameworks like React, Vue.js,
and friends.

I see a lot of very heavy sites using them, but then, my perspective
is very biased as the companies that I work with work with me precisely
because they are facing performance challenges.

I was curious just how common the situation is and just how much of a
penalty we’re paying when we make these frameworks the default starting point.

Thanks to HTTP Archive, we can figure that out.


Introduction en français
===========================

.. seealso::

   - https://javascript.developpez.com/actu/303519/Quels-sont-les-couts-lies-a-l-utilisation-de-frameworks-JavaScript-pour-le-developpement-Web-Une-analyse-des-sites-utilisant-React-Vue-js-ou-Angular/


Quels sont les coûts liés à l'utilisation de frameworks JavaScript pour
le développement Web ?

Cette question a certainement plusieurs fois été abordée, et dans la
plupart des cas, les avis les plus partagés semblent pointer du doigt
les frameworks JavaScript comme étant responsables de la lenteur des
sites Web.

Il est en effet fréquent d'entendre que les performances du Web auraient
baissé ces dernières années à cause de l’avènement des frameworks Web,
comme React et Vue.js, et les applications Web monopage ou SPA (single-page application)
qui privilégient les développeurs à l’expérience utilisateur.

S'invitant dans le débat, Tim Kadlec, un développeur qui aide les
organisations à améliorer les performances de leurs sites, estime pour
sa part qu'il n'y a « pas de moyen plus rapide de ralentir un site que
d'utiliser un tas de JavaScript », et c'est justement ce que font
les frameworks JavaScript : utilisez beaucoup plus de JavaScript.

Mais « le truc avec JavaScript », poursuit-il, « c'est que vous finissez
par payer une taxe sur les performances pas moins de quatre fois », dit-il.

Les quatre taxes auxquelles il fait allusion sont :

- le coût de téléchargement du fichier sur le réseau ;
- le coût de l'analyse et de la compilation du fichier non compressé une fois téléchargé ;
- le coût d'exécution du JavaScript ; et
- le coût de la mémoire.


« La combinaison [de ces quatre taxes] est très coûteuse.

Et nous utilisons de plus en plus de JavaScript sur le Net.

Nous rendons les fonctionnalités de base de nos sites de plus en plus
dépendantes de JavaScript à mesure que les organisations évoluent vers
des sites propulsés par des frameworks tels que React, Vue.js et al »,
dit-il.

Ce ne sont toutefois pas des propos non fondés puisqu'il le montre en
se basant sur une analyse effectuée grâce aux données du site HTTP Archive.

HTTP Archive suit plus de 4 millions d'URL de bureau et plus de 5 millions
d'URL mobiles.

Parmi les nombreux points de données collectés par HTTP Archive, on peut
citer la liste des technologies détectées pour chaque site suivi.

Cela signifie que vous pouvez sélectionner des milliers de sites utilisant
divers frameworks et voir la taille de code JavaScript qu'ils transmettent
et ce que cela coûte au processeur ; ce à quoi Tim Kadlec s'est intéressé.

Il a décidé de comparer les données agrégées de HTTP Archive pour
l'ensemble des sites Web enregistrés et les comparer aux données
spécifiques aux sites utilisant React, Vue.js et Angular.

Il a aussi ajouté jQuery, qui reste une bibliothèque très populaire et
qui représente également une approche de développement JavaScript
différente de l'approche SPA fournie par React, Vue.js et Angular.


L'écart mobile vs bureau
=========================


Tim Kadlec voulait aussi savoir s'il y avait un grand écart entre
l'expérience mobile et l'expérience de bureau.

En analysant les données du point de vue des octets JavaScript
transmis sur le réseau au démarrage, il n'a constaté aucune différence
significative.

Mais en ce qui concerne le temps de traitement, l'écart est important
au détriment des appareils mobiles. « Bien qu'une certaine différence
soit attendue entre mobile et desktop, le fait de constater un écart
important nous indique que ces frameworks ne font pas assez pour
prioriser les appareils moins puissants et aider à combler cet écart »,
dit-il.

Ce qu'il est important de retenir, c'est que jQuery se comporte aussi
bien que l'ensemble des sites.

Mais lorsqu'on passe aux frameworks JavaScript, en particulier Angular,
Vue.js et React, il y a un lourd tribut à payer en termes de performance.

Source : Tim Kadlec
