

.. index::
   pair: Vanilla ; JS Academy


.. _vanilla_js_academy:

===========================================
Vanilla JS academy
===========================================

.. seealso::

   - https://vanillajsacademy.com/

.. contents::
   :depth: 3


What You’ll Learn
====================

- DOM manipulation, injection, and traversal
- How to transform and edit strings, arrays, and objects
- ES6 essentials
- How to structure and organize code
- How to save data locally
- Ajax and HTTP
- How to work with APIs
- How to write JavaScript plugins
- Framework-free web apps
- How to easily write cross-browser compatible code
- How to debug broken code
- Working with polyfills
- JavaScript performance tricks
