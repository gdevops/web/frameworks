.. index::
   pair: Vanilla Javascript ; Framework

.. _vanilla_javascript:

===========================================
Vanilla Javascript
===========================================

.. seealso::

   - https://vanillajsacademy.com/
   - https://gomakethings.com/when-is-vanilla-js-vanilla/
   - :ref:`chris_ferdinandi`


.. toctree::
   :maxdepth: 3

   vanilla_js_academy/vanilla_js_academy
