
.. index::
   pair: Organizations ; small technology FOUNDATION

.. _small_tech:

===========================================
small technology FOUNDATION
===========================================


.. seealso::

   - https://small-tech.org/about/
   - https://small-tech.org/about/#the-foundation
   - https://small-tech.org/about/#small-technology
   - https://small-tech.org/research-and-development/
   - :ref:`laura_kalbag`
   - :ref:`aral_balkan`
   - https://sitejs.org/

.. contents::
   :depth: 3


.. figure:: small_tech_foundation.png
   :align: center

   https://small-tech.org/about/#the-foundation


About
======

We’re a tiny and independent two-person not-for-profit based in Ireland.
We advocate for and build small technology to protect personhood and democracy
in the digital network age.

Read on to learn more about the foundation and small technology.


.. figure:: small_tech_definition.png
   :align: center

   https://small-tech.org/about/#small-technology
