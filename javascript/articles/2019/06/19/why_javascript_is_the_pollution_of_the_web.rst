

.. _javascript_2019_06_19_pollution:

==============================================
Why JavaScript is the pollution of the web
==============================================

.. seealso::

   - https://gomakethings.com/why-javascript-is-the-pollution-of-the-web/
   - https://changelog.com/jsparty/80
   - https://x.com/changelog
   - https://changelog.com/jsparty
   - https://x.com/kbal11

.. contents::
   :depth: 3

Description
=============

Last week, I joined the `JS Party`_ podcast for a chat about vanilla JS, building
a simpler web, and why I think JavaScript is the carbon dioxide of the web.

It was a really great discussion that I think you’ll enjoy a lot.
Click here_ to listen.


.. _here: https://changelog.com/jsparty/80
.. _`JS Party`: https://changelog.com/jsparty

https://changelog.com/jsparty/80
====================================

.. seealso::

   - https://changelog.com/jsparty/80
   - https://github.com/thechangelog/show-notes/blob/master/jsparty/js-party-80.md

KBall, Divya, and Nick get together with Chris Ferdinandi to talk about vanilla
JavaScript, best resources for learning, and our favorite vanilla JavaScript
tips, tricks and APIs.


Featuring
------------

- Chris Ferdinandi (https://x.com/ChrisFerdinandi)
- Kevin Ball (https://x.com/kbal11, https://github.com/kball, https://zendev.com/)
- Divya Sasidharan (https://x.com/shortdiv, https://github.com/shortdiv)
- Nick Nisi  (https://x.com/nicknisi, https://github.com/nicknisi)

Segment 1
------------

-  `Artifact conference <https://artifactconf.com/>`__
-  `The split - article on JS for
   server <https://adactio.com/journal/15050>`__
-  `When is vanilla JS
   vanilla <https://gomakethings.com/when-is-vanilla-js-vanilla/>`__
-  `Svelte <https://svelte.dev/>`__
-  `The “Developer Experience” Bait and
   Switch <https://infrequently.org/2018/09/the-developer-experience-bait-and-switch/>`__

Segment 2
------------

-  `Eloquent JavaScript <https://eloquentjavascript.net/>`__
-  `Wes Bos <https://wesbos.com/>`__
-  `Brad Frost <http://bradfrost.com/>`__
-  `Jeremy Keith <https://adactio.com/>`__
-  `Sara Soueidan <https://www.sarasoueidan.com/>`__
-  `Article on knowing what to focus
   on <https://gomakethings.com/knowing-what-to-focus-on/>`__

Segment 3
-------------

-  `Array
   Reduce <https://gomakethings.com/using-array.reduce-in-vanilla-js/>`__
-  `Polyfill.io <https://polyfill.io/v3/>`__
-  `Eric Elliot <https://medium.com/@_ericelliott>`__
-  `James Sinclair <https://jrsinclair.com/>`__
-  `You Don't Know JS <https://github.com/getify/You-Dont-Know-JS>`__
-  `Learning JavaScript Design
   Patterns <https://addyosmani.com/resources/essentialjsdesignpatterns/book/>`__
-  `Programming as
   Translation <https://increment.com/internationalization/programming-as-translation/>`__
-  `URLSearchParams <https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams>`__
-  `FormData <https://developer.mozilla.org/en-US/docs/Web/API/FormData>`__
